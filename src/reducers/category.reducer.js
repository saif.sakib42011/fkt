// const categoryReducer=()=>{}
// export default categoryReducer

import { categoryConstant } from "../actions/constants"
const initialState = {
    categories: [],
    loading: false,
    error: ''
}

const buildNewCategories = (parentId, categories, category) => {
    // let myCategories = []
    // for (let cat of categories) {

    //     if (cat._id === parentsId) {
    //         myCategories.push({
    //             ...cat,
    //             children: cat.children && cat.children.length > 0 ? 
    //             buildNewCategories(parentsId, [...cat.children, {
    //                 _id: category._id,
    //                 parentId: category.parentId,
    //                 name: category.name,
    //                 slug: category.slug,
    //                 children: category.children
    //             }], category) : []
    //         })
    //     } else {
    //         myCategories.push({
    //             ...cat,
    //             children: cat.children && cat.children.length > 0 ? 
    //             buildNewCategories(parentsId, cat.children, category) : []
    //         })
    //     }

    // }
    // return myCategories

    if (parentId === undefined) {
        return [
            ...categories,
            {
                _id: category._id,
                name: category.name,
                slug: category.slug,
                children: []
            }
        ]
    }
    const cata = categories.map(cat => {
        console.log({ ...cat });
        // console.log(cat);
        if (cat._id === parentId) {

            return {
                ...cat,
                children: cat.children ?
                    buildNewCategories(parentId, [...cat.children, {
                        _id: category._id,
                        name: category.name,
                        slug: category.slug,
                        parentId: category.parentId,
                        children: category.children
                    }], category) : []

            }
        } else {
            return {
                ...cat,
                children: cat.children ?
                    buildNewCategories(parentId, cat.children, category) : []
            }
        }

    })
    // console.log(cata);
    return cata
}

const categoryReducer = (state = initialState, action) => {

    // console.log(action);
    switch (action.type) {
        case categoryConstant.GET_ALL_CATEGORIES_REQUEST:
            state = {
                ...state,
                loading: true,
                // categories: action.payload.categories,
            }
            break;
        case categoryConstant.GET_ALL_CATEGORIES_SUCCESS:
            state = {
                ...state,
                loading: false,
                categories: action.payload.categories,
            }
            break;
        case categoryConstant.GET_ALL_CATEGORIES_FAILURE:
            state = {
                ...initialState,
                error: action.payload.error,
            }
            break;


        case categoryConstant.ADD_NEW_CATEGORY_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;


        case categoryConstant.ADD_NEW_CATEGORY_SUCCESS:
            const category = action.payload.category;
            const updatedCategories = buildNewCategories(category.parentId, state.categories, category)
            console.log(updatedCategories);
            state = {
                ...state,
                categories: updatedCategories,
                loading: false
            }
            break;
        case categoryConstant.ADD_NEW_CATEGORY_FAILURE:
            state = {
                ...initialState,
            }
            break;
        default:
            // state={...initialState}
            break;
    }
    return state;
}

export default categoryReducer
