import {userConstant} from '../actions/constants'

const initialState={
 loading:false
}
const userReducer=(state=initialState,action)=>{
    switch (action.type) {
        case userConstant.USER_REGISTER_REQUEST:
            state={
                ...state,
                loading:true
            }
            break;
        case userConstant.USER_REGISTER_SUCCESS:
            state={
                ...state,
                loading:false,
                success:action.payload.success,
            }
            break;
        case userConstant.USER_REGISTER_FAILURE:
            state={
                ...state,
                loading:false,
                error:action.paylaod.error
            }
            break;    
        default:
            state={
                ...initialState
            }
            break;
    }
    return state
}

export default userReducer