// const productReducer=()=>{}
// export default productReducer

import { productConstant } from "../actions/constants";
const initialState={
    products:[]
}
const productReducer=(state=initialState,action)=>{
    switch (action.type) {
        case productConstant.GET_ALL_PRODUCTS_SUCCESS:
            state={
                ...state,
                products:action.payload.products
            }
            break;
    
        default:
            break;
    }
    return state
}

export default productReducer