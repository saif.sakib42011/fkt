import { mqttConstant } from "../actions/constants"
const initialState = {
    mqtts: [],
    loading: false,
    error: ''
}

const buildNewMqtts = (parentId, mqtts, mqtt) => {
    if (parentId === undefined) {
        return [
            ...mqtts,
            {
                _id: mqtt._id,
                name: mqtt.name,
                slug: mqtt.slug,
                children: []
            }
        ]
    }
    let myMqtts = []
    for (let mqt of mqtts) {
        if (mqt._id === parentId) {
            myMqtts.push({
                ...mqt,
                children: mqt.children ?
                    buildNewMqtts(parentId, [...mqt.children, {
                        _id: mqtt._id,
                        parentId: mqtt.parentId,
                        name: mqtt.name,
                        slug: mqtt.slug,
                        children: mqtt.children
                    }], mqtt) : []
            })
        }
        else {
            myMqtts.push({
                ...mqt,
                children: mqt.children ?
                    buildNewMqtts(parentId, mqt.children, mqtt) : []
            })
        }

    }
    return myMqtts
}


const mqttReducer = (state = initialState, action) => {
    switch (action.type) {
        case mqttConstant.GET_ALL_MQTT_REQUEST:
            state = {
                ...state,
                loading: true,
            }
            break;
        case mqttConstant.GET_ALL_MQTT_SUCCESS:
            state = {
                ...state,
                loading: false,
                mqtts: action.payload.mqtts,
            }
            break;
        case mqttConstant.GET_ALL_MQTT_FAILURE:
            state = {
                // ...state,
                ...initialState,
                error: action.payload.error,
            }
            break;


        case mqttConstant.ADD_NEW_MQTT_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;


        case mqttConstant.ADD_NEW_MQTT_SUCCESS:
            const updatedMqtt = buildNewMqtts(action.payload.mqtt.parentId, state.mqtts, action.payload.mqtt)
            state = {
                ...state,
                mqtts: updatedMqtt,
                loading: false
            }
            break;
        case mqttConstant.ADD_NEW_MQTT_FAILURE:
            state = {
                ...initialState,
            }
            break;
        default:
            // state={...initialState}
            break;
    }
    return state;
}

export default mqttReducer
