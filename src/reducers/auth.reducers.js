import { authConstant } from "../actions/constants";

export const initialState = {

}
const authReducer = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
        case authConstant.LOGIN_REQUEST:
            state = {
                ...state,
                authenticating: true,
                authenticate: false
            }
            break;
        case authConstant.LOGIN_SUCCESS:
            state = {
                ...state,
                token: action.payload.token,
                user: action.payload.user,
                authenticate: true,
                authenticating: false,
            }
            break;



        case authConstant.LOGOUT_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;
        case authConstant.LOGOUT_SUCCESS:
            state = {
                ...initialState,
                loading: false
            }
            break;

        case authConstant.LOGIN_FAILURE:
            state = {
                ...initialState
                // ...state,
                // error:action.payload.error
            }
            break;


        default:
            state = {
                ...initialState,
                authenticate: false
            }
            break;
    }

    return state
}

export default authReducer

