import { userOnlyConstant } from "../actions/constants";
const initialState={
    users:[],
    loading:false
}

const usersOnlyReducer = (state = initialState, action) => {
    switch (action.type) {
        // case userOnlyConstant.GET_USERS_ONLY_REQUEST:
        //     state = {
        //         ...state,
        //         loading: true,
        //     }
        //     break;
        case userOnlyConstant.GET_USERS_ONLY_SUCCESS:
        state = {
            ...state,
            loading: false,
            users:action.payload.users
        }
        break;
        // case userOnlyConstant.GET_USERS_ONLY_FAILURE:
        //     state = {
        //         // ...initialState,
        //     }
        //     break;
        default:
            break;
    }
    return state;
}

export default usersOnlyReducer