import {combineReducers} from "redux"
import authReducer from "./auth.reducers"
import userReducer from "./user.reducer"
import categoryReducer from "./category.reducer"
import orderReducer from "./order.reducer"
import productReducer from "./product.reducer"
import mqttReducer from "./mqtt.reducer"
import usersOnlyReducer from "./useronly.reducer"



 const rootReducer= combineReducers({
     auth:authReducer,
     user:userReducer,
     category:categoryReducer,
     order:orderReducer,
     product:productReducer,
     mqtt:mqttReducer,
     useronly:usersOnlyReducer
 })
export default rootReducer