import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React,{useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {
  Switch,
  Route,

} from "react-router-dom";

import Signin from './containers/Sign in';
import Signup from './containers/sign up';
import Home from './containers/Home';
import PrivateRoute from './components/HOC/privateRoute';
import {  getInitialData, isUserLoggedin } from './actions';
import Products from './containers/products';
import Orders from './containers/orders';
import Category from './containers/category';
import Mqtt from './containers/mqtt';


const App=()=> {
  const dispatch = useDispatch()

  const auth = useSelector(state => state.auth)  
  const category = useSelector(state => state.category)
 
  useEffect(() => {
      if (!auth.authenticate) {
        dispatch(isUserLoggedin())  
      }
      
      if (!category.categories.length) {
        dispatch(getInitialData())
      }
         
  },[auth.authenticate,dispatch,category.categories])
  
  return (
        
          <Switch>
            <PrivateRoute exact path='/' component={Home}/>
            <PrivateRoute  path='/category' component={Category}/>
            <PrivateRoute  path='/products' component={Products}/>
            <PrivateRoute  path='/orders' component={Orders}/>
            <PrivateRoute  path='/mqtt' component={Mqtt}/>
            
            
            <Route  path='/signin'  component={Signin}/>
            <Route  path='/signup'  component={Signup}/>
          </Switch>
        
  );
}

export default App;
