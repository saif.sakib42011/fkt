import axios from "../helpers/axios"
import { userOnlyConstant } from "./constants";

export const getUsersOnly=()=>{
    return async dispatch=>{
        const res=await axios.get('api/auth/admin/getusers');
        const { onlyusers } = res.data
        dispatch({
            type:userOnlyConstant.GET_USERS_ONLY_REQUEST,
        })
        
        console.log(onlyusers);
        if (res.status===200) {
            dispatch({
                type:userOnlyConstant.GET_USERS_ONLY_SUCCESS,
                payload:{users:onlyusers}
            })
        }
        else{
            dispatch({
                type:userOnlyConstant.GET_USERS_ONLY_FAILURE,
                payload: { error: res.data.error }
            })
        } 
    }
}