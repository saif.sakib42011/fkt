import axios from '../helpers/axios'
import { authConstant } from './constants'

export const login = (user) => {
    return async (dispatch) => {
        const res = await axios.post('/api/auth/admin/login', { ...user })
        dispatch({
            type: authConstant.LOGIN_REQUEST,

        })

        if (res.status === 200) {
            const { user, token } = res.data
            localStorage.setItem('token', token)
            localStorage.setItem('user', JSON.stringify((user)))
            dispatch({
                type: authConstant.LOGIN_SUCCESS,
                payload: { token, user }
            })

        } else {
            if (res.status === 401) {
                dispatch({
                    type: authConstant.LOGIN_FAILURE,
                    payload: { error: res.data.error },
                })
            }
        }
    }
}

export const isUserLoggedin = () => {
    return async dispatch => {
        const token = localStorage.getItem("token")
        if (token) {
            const user = localStorage.getItem("user")
            dispatch({
                type: authConstant.LOGIN_SUCCESS,
                payload: { token, user }
            })
        } else {
            dispatch({
                type: authConstant.LOGIN_FAILURE,
                authenticate: false,
                message: "user must be log in"
            })
        }
    }
}


export const signout = () => {
    return async dispatch => {
        dispatch({
            type: authConstant.LOGOUT_REQUEST
        })
        const res = await axios.post('/api/auth/admin/logout')
        if (res.status === 200) {
            localStorage.clear();
            dispatch({
                type: authConstant.LOGOUT_SUCCESS

            })
        }else{
            dispatch({
                type: authConstant.LOGOUT_FAILURE,
                // payload:{error:res.data.error}
            })
        }

    }
}
