import { mqttConstant } from "../actions/constants";
import axios from "../helpers/axios"

export const getAllMqtt = () => {
    return async (dispatch) => {
        const res = await axios.get('/api/mqtt/get')
        console.log(res);
        const { mqttList } = res.data
        
        await dispatch({
            type: mqttConstant.GET_ALL_MQTT_REQUEST,
        })

        if (res.status === 200) {
            dispatch({
                type: mqttConstant.GET_ALL_MQTT_SUCCESS,
                payload: { mqtts: mqttList }
            })
        } else {
            dispatch({
                type: mqttConstant.GET_ALL_MQTT_FAILURE,
                payload: { error: res.data.error }
            })
        }
    }
}

export const addMqtt = (form) => {
    return async (dispatch) => {
        
        dispatch({
            type: mqttConstant.ADD_NEW_MQTT_REQUEST,    
        })
        
        const res = await axios.post('api/mqtt/createmqtt',form);
        console.log(res);
        
            
        if (res.status === 201) {
            await dispatch({
                type: mqttConstant.ADD_NEW_MQTT_SUCCESS,
                payload: { mqtt: res.data.mqttdata }
            })
        }else{
            await dispatch({
                type: mqttConstant.ADD_NEW_MQTT_FAILURE,
                payload: { error: res.data.error }
            })
        }
    }
}