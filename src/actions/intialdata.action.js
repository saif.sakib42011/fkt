import { categoryConstant,productConstant } from "./constants"
import axios from "../helpers/axios";
export const getInitialData=()=>{
    return async dispatch=>{
        
        const res=await axios.post("/api/initialdata",{});
        const {categories,products} = res.data
        if (res.status===200) {
            dispatch({
                type:categoryConstant.GET_ALL_CATEGORIES_SUCCESS ,
                payload:{categories}
            });
            dispatch({
                type:productConstant.GET_ALL_PRODUCTS_SUCCESS ,
                payload:{products}
            })  
        }
        

    }
}