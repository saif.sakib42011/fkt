import axios from '../helpers/axios'
import { userConstant } from './constants'

export const signup = (user) => {
    return async (dispatch) => {
        dispatch({
            type: userConstant.USER_REGISTER_REQUEST,
        })
        const res = await axios.post('/api/auth/admin/register', { ...user })
        if (res.status === 201) {
            const { user, success } = res.data
            // localStorage.setItem('token', token)
            // localStorage.setItem('user', JSON.stringify((user)))
            dispatch({
                type: userConstant.USER_REGISTER_SUCCESS,
                payload: { success, user }
            })

        } else {
            if (res.status === 500) {
                dispatch({
                    type: userConstant.USER_REGISTER_FAILURE,
                    payload: { error: res.data.error },
                })
            }
        }
    }
}
