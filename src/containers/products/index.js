import React, { useState } from 'react'
import Layout from '../../components/Layout'
import { Container, Row, Col, Table } from "react-bootstrap"
import Input from '../../components/Ui/input'
import { useDispatch, useSelector } from 'react-redux'
import NewModal from '../../components/Ui/Modal'
import './styles.css'
import { addProduct } from '../../actions'
import { generatePublicUrl } from '../../urlConfig'
export default function Products() {

    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    const product = useSelector(state => state.product)
    //useState
    const [name, setName] = useState('')
    const [quantity, setQuantity] = useState('')
    const [price, setPrice] = useState('')
    const [description, setDescription] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [productPictures, setProductPictures] = useState('')
    const [show, setShow] = useState(false);
    const [productDetailModal, setProductDetailModal] = useState(false);
    const [productDetails, setProductDetails] = useState(null);

    

    //funcs
    const handleClose = () => {
        const form = new FormData();
        form.append("name", name);
        form.append("quantity", quantity);
        form.append("price", price);
        form.append("description", description);
        form.append("category", categoryId);
        for (let pic of productPictures) {
            form.append("productPictures", pic);
        }
        dispatch(addProduct(form))
        setShow(false);
    };
    const handleShow = () => setShow(true);

    const createCategory = (categories, options = []) => {

        for (let cat of categories) {
            options.push({ value: cat._id, name: cat.name })
            if (cat.children.length > 0) {
                createCategory(cat.children, options)
            }

        }
        return options

    }

    const handleProductImage = (e) => {
        setProductPictures([
            ...productPictures,
            e.target.files[0]
        ])
    }


    const handleCloseProductdetailModal = () => {
        setProductDetailModal(false)
    }
    const renderTable = () => {
        return (<Table style={{ fontSize: '12px' }} responsive="sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Description</th>
                    <th>Product Pictures</th>
                    <th>Category</th>
                </tr>
            </thead>
            <tbody>
                {product.products.map(product => {
                    return <tr onClick={() => showProductDetailModal(product)} key={product._id}>
                        <td>1</td>
                        <td>{product.name}</td>
                        <td>{product.price}</td>
                        <td>{product.quantity}</td>
                        <td>{product.description}</td>
                        {/* <td>{product.productPictures}</td> */}
                        {/* <td>{product.name}</td> */}
                    </tr>
                })}
            </tbody>
        </Table>

        )
    }
    const renderAddProductModal = () => {
        return <NewModal title="Add Product" show={show} handleClose={handleClose}>
            <Input
                value={name}
                placeholder='product name'
                onChange={e => { setName(e.target.value) }}
                label="name"
            />
            <Input
                value={quantity}
                placeholder='quantity'
                onChange={e => { setQuantity(e.target.value) }}
                label="quantity"
            />
            <Input
                value={price}
                placeholder='price'
                onChange={e => { setPrice(e.target.value) }}
                label="price"
            />
            <Input
                value={description}
                placeholder='description'
                onChange={e => { setDescription(e.target.value) }}
                label="description"

            />

            <select
                value={categoryId}
                onChange={e => { setCategoryId(e.target.value) }}
                className="form-control">
                <option>Select Products</option>
                {createCategory(category.categories).map(option => {
                    return <option key={option.value} value={option.value}> {option.name}</option>
                })}
            </select>

            {
                productPictures.length > 0 ?
                    productPictures.map((pic, idx) => {
                        return <div key={idx}>{pic.name}</div>
                    }) : null
            }

            <Input
                type="file"
                name="productPictures"
                placeholder="Product Pictures"
                onChange={handleProductImage}

            />
        </NewModal>

    }
    const showProductDetailModal = (product) => {
        setProductDetailModal(true)
        setProductDetails(product)
        // console.log(productDetails)
        // console.log(product)

    }

    const renderProductDetailModal = () => {
        if (!productDetails) {
            return null
        }
        return <NewModal
            size="lg"
            show={productDetailModal}
            handleClose={handleCloseProductdetailModal}
            title='product details'
        >
            <Row>
                <Col md={6}>
                    <label className='key'>Name</label>
                    <p className='value'>{productDetails.name}</p>
                </Col>
                <Col md={6}>
                    <label className='key'>price</label>
                    <p className='value'>{productDetails.price}</p>
                </Col>
                <Col md={6}>
                    <label className='key'>quantity</label>
                    <p className='value'>{productDetails.quantity}</p>
                </Col>
                <Col md={6}>
                    <label className='key'>category</label>
                    <p className='value'>{productDetails.category.name}</p>
                </Col>
                <Col md={12}>
                    <label className='key'>description</label>
                    <p className='value'>{productDetails.description}</p>
                </Col>
            </Row>
            <Row>
                <Col style={{ display: "flex" }}>
                    {productDetails.productPictures.map(pic => {
                        return <div key={pic._id} className="imgContainer">
                            <img src={generatePublicUrl(pic.img)}
                                alt={pic.img} />
                        </div>
                    })}
                </Col>
            </Row>
        </NewModal>
    }
    return (
        <div>
            <Layout sidebar>
                <Container>
                    <Row>
                        <Col md={12}>
                            <div className='d-flex justify-content-between'>
                                <h3>Products</h3>
                                <button onClick={handleShow}>Add</button>

                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            {renderTable()}
                        </Col>
                    </Row>
                </Container>
                {renderAddProductModal()}
                {renderProductDetailModal()}
            </Layout>
        </div>
    )
}