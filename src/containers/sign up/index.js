import React,{useState} from "react";
import Layout from "../../components/Layout";
import { Form, Button, Col, Row, Container } from "react-bootstrap";
import Input from "../../components/Ui/input";
import { useSelector,useDispatch } from "react-redux";
import { Redirect } from "react-router";
import {signup} from "../../actions/" 
const Signup = () => {

  const dispatch = useDispatch()
  
  const [firstname, setFirstname] = useState("")
  const [lastname, setLastname] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const auth = useSelector(state => state.auth)
  const user = useSelector(state => state.user)
  
  console.log(user.loading);
  if (user.loading) {
    return <p>loading...</p>
  }

  if (auth.authenticate) {
    return <Redirect to='/' />

  }

  const userRegister=(e)=>{
    e.preventDefault()
    const user={
      firstname,lastname,email,password
    }
    dispatch(signup(user))
    setFirstname('')
    setLastname('')
    setEmail('')
    setPassword('')
  
  }
  



  return (
    <Layout>
      <Container>
        <Row style={{ marginTop: "50px" }}>
          <Col md={{ span: 6, offset: 3 }}>
            <Form onSubmit={userRegister} className="container">
              <Row>
                <Col md={{ span: 6 }}>
                  <Input
                    value={firstname}
                    onChange={e=>setFirstname(e.target.value)}
                    label="First name"
                    type="text"
                    placeholder="Enter first name"
                  />
                </Col>

                <Col md={{ span: 6 }}>
                  <Input
                    value={lastname}
                    onChange={e=>setLastname(e.target.value)}
                    label="Last name"
                    type="text"
                    placeholder="Enter last name"
                    
                  />
                </Col>
              </Row>
              <Input
                value={email}
                onChange={e=>setEmail(e.target.value)}
                label="Email"
                type="email"
                placeholder="Enter Email"
                
              />

              <Input
                value={password}
                onChange={e=>setPassword(e.target.value)}
                label="Password"
                type="Password"
                placeholder="Enter Password"
                
              />

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Signup;
