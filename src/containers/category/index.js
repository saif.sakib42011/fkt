import React, { useState } from 'react'
import Layout from '../../components/Layout'
import { Container, Col, Row } from "react-bootstrap";
import NewModal from "../../components/Ui/Modal";
import Input from "../../components/Ui/input";
import { useDispatch, useSelector } from "react-redux";
import { addCategory, updateCategories,deleteCategories as deleteCategoriesAction, getAllCategory } from '../../actions/category.action';
//checkbox-tree
import CheckboxTree from 'react-checkbox-tree';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
//checkbox-tree
import { IoIosCheckboxOutline, IoIosCheckbox, IoIosArrowForward, IoIosArrowDown } from "react-icons/io";


export default function Category() {
    //useState
    const [show, setShow] = useState(false);
    const [categoryName, setCategoryName] = useState('');
    const [parentCategoryId, setParentCategoryId] = useState('');
    const [categoryImage, setCategoryImage] = useState('');

    const [checked, setChecked] = useState([])
    const [checkedArray, setCheckedArray] = useState([])
    const [expanded, setExpanded] = useState([])
    const [expandedArray, setExpandedArray] = useState([])
    const [updateCategoryModal, setUpdateCategoryModal] = useState(false)
    const [deleteCategoryModal, setDeleteCategoryModal] = useState(false)

    //react-redux
    const category = useSelector(state => state.category);
    const dispatch = useDispatch()

    //functions
    const handleClose = () => {
        const form = new FormData();
        form.append("name", categoryName);
        form.append("parentId", parentCategoryId);
        form.append("categoryImage", categoryImage);
        dispatch(addCategory(form))
        setShow(false)
    };
    const handleShow = () => setShow(true);

    const createCategory = (categories, options = []) => {
        for (let cat of categories) {
            options.push({
                value: cat._id,
                name: cat.name,
                parentId: cat.parentId
            })
            if (cat.children.length > 0) {
                createCategory(cat.children, options)
            }
        }
        return options
    }
    
    const renderCategory = (categories) => {
        let mycategories = [];
        for (let cat of categories) {
            mycategories.push({
                value: cat._id,
                label: cat.name,
                children: cat.children.length > 0 && renderCategory(cat.children)

                // <li key={cat.name}>{cat.name}
                //     {cat.children.length > 0 ? (<ul>{renderCategory(cat.children)}</ul>) : null}
                // </li>
            })
        }

        return mycategories;
    }
    
    const updateExpandedAndCheckedCategory = ()=> {
        const categories = createCategory(category.categories);
        const checkedArray = [];
        const expandedArray = [];

        checked.length > 0 && checked.forEach((categoryId, idx) => {
            const category = categories.find((category, _idx) => categoryId === category.value);
            checkedArray.push(category)
        })

        expanded.length > 0 && expanded.forEach((categoryId, idx) => {
            const category = categories.find((category, _idx) => categoryId === category.value);
            console.log(categories);
            expandedArray.push(category)
        })

        setExpandedArray(expandedArray)
        setCheckedArray(checkedArray)
    }
    const updateCategory = () => {
        updateExpandedAndCheckedCategory()
        setUpdateCategoryModal(true);    
    }

    const handleCategoryInput = (key, value, idx, type) => {
        if (type === 'checked') {
            const updatedCheckedArray = checkedArray.map((item, _idx) => {
                return idx === _idx ? { ...item, [key]: value } : item
            })
            setCheckedArray(updatedCheckedArray)
        } else if (type === 'expanded') {
            const updatedExpandedArray = expandedArray.map((item, _idx) => {
                return idx === _idx ? { ...item, [key]: value } : item
            })
            setExpandedArray(updatedExpandedArray);
        }
    }
    
    const updateCategoryForm =async () => {
        const form = new FormData();
        checkedArray.forEach((item, _idx) => {
            form.append('_id', item.value)
            form.append('name', item.name)
            form.append('parentId', item.parentId ? item.parentId : "")
            form.append('type', item.type)
        })
        expandedArray.forEach((item, _idx) => {
            form.append('_id', item.value)
            form.append('name', item.name)
            form.append('parentId', item.parentId ? item.parentId : "")
            form.append('type', item.type)
        })
        await dispatch(updateCategories(form))
        try {
            dispatch(getAllCategory())
        } catch (error) {
            console.log(error);
        }
        setUpdateCategoryModal(false)
    }
    
    const renderUpdateCategoryModal=()=>{
        return (
            <NewModal
                title='Update Category'
                show={updateCategoryModal}
                handleClose={updateCategoryForm}
                size='lg'>

                <h6>Expanded</h6>
                {expandedArray.length > 0 &&
                    expandedArray.map((item, idx) => {
                        return <Row key={idx}>

                            <Col md={4}>
                                <Input
                                    value={item.name}
                                    placeholder="Category name"
                                    onChange={e => handleCategoryInput('name', e.target.value, idx, 'expanded')}
                                />
                            </Col>

                            <Col md={4}>
                                <select
                                    className="form-control"
                                    value={item.parentId}
                                    placeholder="Category name"
                                    onChange={e => handleCategoryInput('parentId', e.target.value, idx, 'expanded')}
                                >

                                    <option>select option</option>
                                    {createCategory(category.categories).map(option => {

                                        return <option key={option.name} value={option.value}>{option.name}</option>
                                    })}
                                </select>
                            </Col>

                            <Col md={4}>
                                <select className='form-control'>
                                    <option>Select Type</option>
                                    <option value='store'>store</option>
                                    <option value='product'>product</option>
                                    <option value='page'>page</option>
                                </select>
                            </Col>
                        </Row>

                    })
                }

                {/* <Input

                    type="file"
                    value={categoryImage}
                    placeholder="Category Image"
                    onChange={e => setCategoryImage(e.target.files[0])}
                /> */}

                <h6>Checked</h6>
                {checkedArray.length > 0 &&
                    checkedArray.map((item, idx) => {
                        return <Row key={idx}>

                            <Col>
                                <Input
                                    value={item.name}
                                    placeholder="Category name"
                                    onChange={e => handleCategoryInput('name', e.target.value, idx, 'checked')}
                                />
                            </Col>
                            <Col>
                                <select
                                    className="form-control"
                                    value={item.parentId}
                                    placeholder="Category name"
                                    onChange={e => handleCategoryInput('parentId', e.target.value, idx, 'checked')}
                                >

                                    <option>select option</option>
                                    {createCategory(category.categories).map(option => {

                                        return <option key={option.name} value={option.value}>{option.name}</option>
                                    })}
                                </select>

                            </Col>

                            <Col>
                                <select className='form-control'>
                                    <option>Select Type</option>
                                    <option value='store'>store</option>
                                    <option value='product'>product</option>
                                    <option value='page'>page</option>
                                </select>
                            </Col>
                        </Row>

                    })
                }

                {/* <Input

                    type="file"
                    value={categoryImage}
                    placeholder="Category Image"
                    onChange={e => setCategoryImage(e.target.files[0])}
                /> */}
            </NewModal>


        )
    }
    
    const deleteCategory= () => {
        updateExpandedAndCheckedCategory()
        setDeleteCategoryModal(true)
    }

    const deleteCategories=async () => {
        const checkedIdsArray= checkedArray.map((item,idx)=>{
            return {id:item.value}
        })
        const expandedIdsArray= expandedArray.map((item,idx)=>{
            return {id:item.value}
        })
        const IdsArray = expandedIdsArray.concat(checkedIdsArray)
        await dispatch(deleteCategoriesAction(IdsArray))
        try {
            dispatch(getAllCategory())
        } catch (error) {
            console.log(error);
        }
        setDeleteCategoryModal(false)
    }
    const renderDeleteCategoryModal=()=>{
        return (
            <NewModal title='Delete Category' show={deleteCategoryModal} handleClose={()=>setDeleteCategoryModal(false)}
             buttons={[
                 {
                    label:"Yes",
                    color:"danger",
                    onClick:deleteCategories
                 },
                 {
                    label:"No",
                    color:"primary",
                    onClick:()=>{
                        alert("getting back to caregory page?")
                    }
                 }
             ]}       
            >
                Are You Sure???
                <h6>expanded</h6>
                {expandedArray.map((item,idx)=>{
                    return (
                        <span key={item.value}>{item.name}</span>
                    )
                })}
                
                <h6>Checked</h6>
                {checkedArray.map((item,idx)=>{
                    return (
                        <span key={item.value}>{item.name}</span>
                    )
                })}
            </NewModal>
        )
    }
    
    return (
        <Layout sidebar>
            <Container>
                <Row>
                    <Col md={12}>
                        <div className='p-2 d-flex justify-content-between'>
                            <h3>Category</h3>
                            <button onClick={handleShow}>Add</button>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col md={12}>
                        {/* <ul>
                            {renderCategory(category.categories)}
                        </ul> */}
                        <CheckboxTree
                            nodes={renderCategory(category.categories)}
                            checked={checked}
                            expanded={expanded}
                            onCheck={checked => setChecked(checked)}
                            onExpand={expanded => setExpanded(expanded)}
                            icons={{
                                check: <IoIosCheckbox />,
                                uncheck: <IoIosCheckboxOutline />,
                                halfCheck: <IoIosCheckbox />,
                                expandClose: <IoIosArrowForward />,
                                expandOpen: <IoIosArrowDown />,
                            }}

                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <button onClick={deleteCategory}>Delete</button>
                        <button onClick={updateCategory}>Edit</button>
                    </Col>
                </Row>
            </Container>
            <NewModal title='Add New Category' show={show} handleClose={handleClose}>
                <Input
                    value={categoryName}
                    placeholder="Category name"
                    onChange={e => setCategoryName(e.target.value)}
                />

                <select
                    className="form-control"
                    value={parentCategoryId}
                    placeholder="Category name"
                    onChange={e => setParentCategoryId(e.target.value)}
                >
                    <option>select option</option>
                    {createCategory(category.categories).map(option => {
                        return <option key={option.name} value={option.value}>{option.name}</option>
                    })}
                </select>

                <Input
                    type="file"
                    value={categoryImage}
                    placeholder="Category Image"
                    onChange={e => setCategoryImage(e.target.files[0])}
                />
            </NewModal>

            {/* UpdateModal */}
            {renderUpdateCategoryModal()}
            
            {/* deleteModal */}
            {renderDeleteCategoryModal()}
        </Layout>
    )
}
