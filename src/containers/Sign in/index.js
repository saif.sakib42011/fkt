import React, { useState } from "react";
import Layout from "../../components/Layout";
import { Form, Button, Col, Row, Container } from "react-bootstrap";
import Input from "../../components/Ui/input";
import { login } from '../../actions';

import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router";
const Signin = () => {

  // const [error, setError] = useState('')



  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  if (auth.authenticate) {
    return <Redirect to='/' />
   
  }  
  const userLogin = (e) => {
    e.preventDefault();
    const user = {
      email,
      password
    }
    dispatch(login(user))
  }


 

  return (

    <Layout>
      <Container>
        <Row style={{ marginTop: "50px" }}>
          <Col md={{ span: 6, offset: 3 }}>
            <Form className="container" onSubmit={userLogin}>
              <Input
                value={email}
                label="Email"
                type="email"
                placeholder="Enter Email"
                onChange={(e) => { setEmail(e.target.value) }}
              />

              <Input
                value={password}
                label="Password"
                type="Password"
                placeholder="Enter Password"
                onChange={(e) => { setPassword(e.target.value) }}
              />

              <Button variant="primary" type="submit" >
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Signin;
