import React from 'react'
import NewModal from "../../../components/Ui/Modal";
import Input from "../../../components/Ui/input";
function AddMqttModal({ title, show, handleClose, mqttName, setMqttName,
    parentMqttId, setParentMqttId, mqttList, userOnlyId, setUserOnlyId, 
    usersOnlyList ,mqttImage,setMqttImage }) {

    return (
        <NewModal title={title} show={show} handleClose={handleClose}>

            <Input
                value={mqttName}
                placeholder="Mqqt title"
                onChange={e => setMqttName(e.target.value)}
            />

            <select
                className="form-control"
                value={parentMqttId}
                placeholder="Category name"
                onChange={e => setParentMqttId(e.target.value)}
            >
                <option>select option</option>
                {mqttList.map(option => {
                    return <option key={option.value} value={option.value}>{option.name}</option>
                })}
            </select>

            <select
                className="form-control mt-3"
                value={userOnlyId}
                placeholder="Category name "
                onChange={e => setUserOnlyId(e.target.value)}
            >
                <option>select user</option>
                {usersOnlyList.map(option => {
                    return <option key={option.value} value={option.value}>{option.name}</option>
                })}
            </select>

            <Input
                type="file"
                value={mqttImage}
                placeholder="Category Image"
                onChange={e => setMqttImage(e.target.files[0])}
            />
        </NewModal>

    )
}

export default AddMqttModal