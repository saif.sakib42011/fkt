import React, { useState,useEffect } from 'react'
import Layout from '../../components/Layout'
import { Container, Col, Row } from "react-bootstrap";
import { addMqtt, getAllMqtt,getUsersOnly } from '../../actions/index';
import { useDispatch, useSelector } from "react-redux";
import { IoIosCheckboxOutline, IoIosCheckbox, IoIosArrowForward, IoIosArrowDown } from "react-icons/io";

import CheckboxTree from 'react-checkbox-tree';
import AddMqttModal from '../mqtt/component/addMqttModal';




function Mqtt() {

    const [show, setShow] = useState(false);
    const [mqttName, setMqttName] = useState('');
    const [parentMqttId, setParentMqttId] = useState('');
    const [userOnlyId, setUserOnlyId] = useState('');
    const [mqttImage, setMqttImage] = useState('');

    const [checked, setChecked] = useState([])
    const [checkedArray, setCheckedArray] = useState([])
    const [expanded, setExpanded] = useState([])
    const [expandedArray, setExpandedArray] = useState([])

    
    //react-redux
    const dispatch = useDispatch()
    const mqtt = useSelector(state => state.mqtt);
    const useronly = useSelector(state => state.useronly);
    // console.log(useronly);
    useEffect(() => {
        if (!mqtt.mqtts.length) {
            dispatch(getAllMqtt())
        }
        
        
    }, [dispatch,mqtt.mqtts])

    useEffect(() => {
        if (!useronly.users.length) {
            dispatch(getUsersOnly())            
        }
    }, [dispatch,
        useronly.users
    ])
    
    
    //functions
    const handleClose = (e) => {
        // e.preventDefault();
        // const form = new FormData();
        // form.append("name", mqttName);
        // form.append("parentId", parentMqttId);
        // form.append("user", userOnlyId);
        // form.append("mqttImage", mqttImage);
        const form={name:mqttName,parentId:parentMqttId,user:userOnlyId}
        // console.log(form);
        dispatch(addMqtt(form))
        setShow(false)
    };
    const handleShow = () => setShow(true);
    
    const createMqtts = (mqtts, options = []) => {
        for (let mqt of mqtts) {
            options.push({
                value: mqt._id,
                name: mqt.name,
                parentId: mqt.parentId,
            })
            if (mqt.children.length > 0) {
                createMqtts(mqt.children, options)
            }
        }
        return options
    }
    const createUsers = (users, options = []) => {
        for (let user of users) {
            options.push({
                value: user._id,
                name: user.firstname+" "+user.lastname,
            })
            
        }
        return options
    }
    const renderMqtt = (mqtts) => {
        let myMqtts = [];
        for (let mqt of mqtts) {
            // myMqtts.push({
            //     value: mqt._id,
            //     label: mqt.name,
            //     children: mqt.children.length > 0 ? renderMqtt(mqt.children) : null
            // })

            myMqtts.push(
                <li key={mqt._id}>{mqt.name}
                {mqt.children.length>0 ? (<ul>{renderMqtt(mqt.children)}</ul>):null}
                </li>          
            )

        }
        return myMqtts
    }
    return (
        <Layout sidebar>
            <Container>
                <Row>
                    <Col md={12}>
                        <div className='p-2 d-flex justify-content-between'>
                            <h3>Mqtt Data</h3>
                            <button onClick={handleShow}>Add</button>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <ul>
                            {renderMqtt(mqtt.mqtts)}
                        </ul>
                        {/* <CheckboxTree
                            nodes={renderMqtt(mqtt.mqtts)}
                            checked={checked}
                            expanded={expanded}
                            onCheck={checked => setChecked(checked)}
                            onExpand={expanded => setExpanded(expanded)}
                            icons={{
                                check: <IoIosCheckbox />,
                                uncheck: <IoIosCheckboxOutline />,
                                halfCheck: <IoIosCheckbox />,
                                expandClose: <IoIosArrowForward />,
                                expandOpen: <IoIosArrowDown />,
                            }}

                        /> */}
                    </Col>
                </Row>

            </Container>
            
            <AddMqttModal
                title='Add New MqttData'
                show={show}
                handleClose={handleClose}
                mqttName={mqttName}
                setMqttName={setMqttName}
                parentMqttId={parentMqttId}
                setParentMqttId={setParentMqttId}
                userOnlyId={userOnlyId}
                setUserOnlyId={setUserOnlyId}
                mqttList={createMqtts(mqtt.mqtts)}
                usersOnlyList={createUsers(useronly.users)}
                mqttImage={mqttImage}
                setMqttImage={setMqttImage}
            />
        </Layout>
    )
}

export default Mqtt
