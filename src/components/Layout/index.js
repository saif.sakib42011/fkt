import React from 'react'
import Header from '../Header'
import { Container, Row, Col } from "react-bootstrap"
import { NavLink } from 'react-router-dom'

const Layout = ({ children, sidebar }) => {
    return (
        <div>
            <Header />

            {sidebar ? <Container fluid>
                <Row>
                    <Col className="sidebar" md={{ span: 2 }}>
                        <ul>
                            <li><NavLink to='/'>Home</NavLink></li>
                            <li><NavLink to='/category'>Category</NavLink></li>
                            <li><NavLink to='/products'>Products</NavLink></li>
                            <li><NavLink to='/orders'>Orders</NavLink></li>
                            <li><NavLink to='/mqtt'>Mqtt</NavLink></li>
                        </ul>
                    </Col>
                    <Col style={{ marginLeft: 'auto', paddingTop:'60px' }} md={{ span: 10 }}>{children}</Col>
                </Row>
            </Container> : children
            }

        </div>
    )
}

export default Layout



