import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { NavLink, Link } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { signout } from "../../actions/";

const Header = () => {
  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)
  
  const logout=()=>{
    dispatch(signout());
  }
  const RenderNotLoggedIn = () => {
    
    return (
      <Nav>
        <li className="nav-item">
          <NavLink to="/signin" className="nav-link">
            Login
                  </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/signup" className="nav-link">
            Register
          </NavLink>
        </li>
      </Nav>
    )
  }
  const RenderLoggedIn = () => {
     
    return (
      
      <Nav>
        <li className="nav-item">
          <span onClick={logout}  style={{cursor:'pointer'}} className="nav-link">
            Logout
          </span>
        </li>

      </Nav>
    )
  }
  return (

    <Navbar fixed='top' style={{ zIndex: "1" }} collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container fluid>
        <Link to="/" className="navbar-brand" href="#home">
          Admin Dashboard
          </Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {/* <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                    </NavDropdown> */}
          </Nav>
          {auth.authenticate?RenderLoggedIn():RenderNotLoggedIn()}          
        </Navbar.Collapse>
      </Container>
    </Navbar>

  );
};

export default Header;
