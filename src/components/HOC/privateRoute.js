import React from 'react';
import { Redirect, Route } from 'react-router';

const PrivateRoute=({component:Component,...rest})=>{
    return <Route  {...rest} component={(props)=>{
        if (localStorage.getItem('token')) {
            return <Component {...props}/>
        }
        
        return <Redirect to ='/signin'/>

    }}/>
}

export default PrivateRoute

