import React from 'react'


import { Modal, Button } from "react-bootstrap"
export default function NewModal({ size, title, handleClose, show, children, buttons }) {
    return (
        <Modal size={size} show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer>
                {buttons ?
                    buttons.map((button, idx) => {
                        return (<Button
                            key={idx}
                            variant={button.color}
                            onClick={button.onClick}
                        >
                            {button.label}
                        </Button>)
                    }):<span>
 
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                      </Button>
                      <Button variant="primary" onClick={handleClose}>
                        Save Changes
                      </Button>
                     
                    </span>
                     
                     }
    
            
  
            </Modal.Footer>

        </Modal>
    )
}
