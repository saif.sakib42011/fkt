import React from 'react'
import { Form } from "react-bootstrap";
export default function Input({controlId, label, type, placeholder, onChange, value }) {
    return (
        <Form.Group>
            {label && <Form.Label>{label}</Form.Label>}
            <Form.Control value={value} onChange={onChange} type={type} placeholder={placeholder} />
        </Form.Group>
    )
}
